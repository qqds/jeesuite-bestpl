/**
 * 
 */
package com.jeesuite.bestpl.message;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;

import com.jeesuite.bestpl.dto.Message;
import com.jeesuite.bestpl.service.MessageService;
import com.jeesuite.common.json.JsonUtils;
import com.jeesuite.kafka.handler.MessageHandler;
import com.jeesuite.kafka.message.DefaultMessage;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @date 2016年6月25日
 */
public class MessageRecvHandler implements MessageHandler {

	@Autowired
	private MessageService messageService;

	@Override
	public void p1Process(DefaultMessage message) {
		
	}

	@Override
	public void p2Process(DefaultMessage message) {
		Serializable body = message.getBody();
		Message msg = JsonUtils.toObject(body.toString(), Message.class);
		messageService.addMessage(msg);
	}


	@Override
	public boolean onProcessError(DefaultMessage message) {
		return true;
	}

}
