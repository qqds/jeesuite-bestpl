package com.jeesuite.bestpl.event;

import com.google.common.eventbus.Subscribe;
import com.jeesuite.bestpl.BestplConstants;
import com.jeesuite.bestpl.dto.RewardAddParam;
import com.jeesuite.common.json.JsonUtils;
import com.jeesuite.kafka.spring.TopicProducerSpringProvider;

public class RewardEventListener {

	private TopicProducerSpringProvider topicProducerProvider; 
	private RewardAddParam param;
	
	
	
	public RewardEventListener(TopicProducerSpringProvider topicProducerProvider) {
		this.topicProducerProvider = topicProducerProvider;
	}

	@Subscribe  
    public void listen(RewardAddParam param) {  
		this.param = param;  
        //通过kafka发送
        topicProducerProvider.publishNoWrapperMessage(BestplConstants.REWARD_ADD_TOPIC, JsonUtils.toJson(param));
    }

	public RewardAddParam getParam() {
		return param;
	}

	public void setParam(RewardAddParam param) {
		this.param = param;
	}

	
	
	
}
